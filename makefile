#LINK_TARGET = bin/sumiggbzo

all : gpu

cpu : comp_cpu
	nvcc bin/cpu/*.o -L$(CWPROOT)/lib -L$(CUDA_PATH)/lib64 -lsu -lpar -lcwp -lm -lcudart -lcuda -o bin/cpu/sumiggbzo_cpu

gpu : comp_gpu
	nvcc bin/gpu/*.o -L$(CWPROOT)/lib -L$(CUDA_PATH)/lib64 -lsu -lpar -lcwp -lm -lcudart -lcuda -o bin/gpu/sumiggbzo_gpu

comp_gpu :
	nvcc -c -I$(CWPROOT)/include -Iinclude -O -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -DCWP_LITTLE_ENDIAN -DSUXDR  -D_BSD_SOURCE -D_POSIX_SOURCE src/gpu/*.c*
	mv *.o bin/gpu/

comp_cpu :
	nvcc -c -I$(CWPROOT)/include -Iinclude -O -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -DCWP_LITTLE_ENDIAN -DSUXDR  -D_BSD_SOURCE -D_POSIX_SOURCE src/cpu/*.c*
	mv *.o bin/cpu/

clean_cpu :
	rm bin/cpu/*.o
	rm bin/cpu/*cpu

clean_gpu :
	rm bin/gpu/*.o
	rm bin/gpu/*gpu
