#include "su.h"
#include <cuda.h>
/* datatypes extracted from mian file and used in seperate files */

/* Ray types */
/* one step along ray */
typedef struct __attribute__((packed)) RayStepStruct {
	float t;		/* time */
	float a;		/* angle */
	float x,z;		/* x,z coordinates */
	float q1,p1,q2,p2;	/* Cerveny's dynamic ray tracing solution */
	int kmah;		/* KMAH index */
	float c,s;		/* cos(angle) and sin(angle) */
	float v,dvdx,dvdz;	/* velocity and its derivatives */
} RayStep;

/* circle for efficiently finding nearest ray step */
typedef struct __attribute__((packed)) CircleStruct {
	int irsf;               /* index of first raystep in circle */
	int irsl;               /* index of last raystep in circle */
	float x;                /* x coordinate of center of circle */
	float z;                /* z coordinate of center of circle */
	float r;                /* radius of circle */
} Circle;

/* one ray */
typedef struct __attribute__((packed)) RayStruct {
	int nrs;		/* number of ray steps */
	RayStep *rs;		/* array[nrs] of ray steps */
	int nc;			/* number of circles */
	int ic;			/* index of circle containing nearest step */
	Circle *c;		/* array[nc] of circles */
} Ray;

/* interpolator for velocity function v(x,z) of two variables */
typedef struct Vel2Struct {
	int nx;		/* number of x samples */
	int nz;		/* number of z samples */
	int nxm;	/* number of x samples minus LTABLE */
	int nzm;	/* number of x samples minus LTABLE */
	float xs,xb,zs,zb,sx[3],sz[3],**vu;
} Vel2;

/* Ray functions */
Ray *makeRay (float x0, float z0, float a0, int nt, float dt, float ft,
	int nx, float dx, float fx, int nz, float dz, float fz, float **v);
void freeRay (Ray *ray);
int nearestRayStep (Ray *ray, float x, float z);

/* Velocity functions */
Vel2* vel2Alloc (int nx, float dx, float fx,
	int nz, float dz, float fz, float **v);
void vel2Free (void *vel2);
void vel2Interp (Vel2 *vel2, float x, float z,
	float *v, float *vx, float *vz, float *vxx, float *vxz, float *vzz);

/* Beam functions */
void formBeams (float bwh, float dxb, float fmin,
	int nt, float dt, float ft, 
	int nx, float dx, float fx, float **f,
	int ntau, float dtau, float ftau, 
	int npx, float dpx, float fpx, float **g);
void accBeam (Ray *ray, float fmin, float lmin,
	int nt, float dt, float ft, float *f,
	int nx, float dx, float fx, int nz, float dz, float fz, float **g);

/* functions defined and used internally */
Circle *makeCircles (int nc, int nrs, RayStep *rs);

/* Beam subroutines */
/* size of cells in which to linearly interpolate complex time and amplitude */
#define CELLSIZE 8

/* factor by which to oversample time for linear interpolation of traces */
#define NOVERSAMPLE 4

/* number of exponential decay filters */
#define NFILTER 6

/* exp(EXPMIN) is assumed to be negligible */
#define EXPMIN (-5.0)

/* filtered complex beam data as a function of real and imaginary time */
typedef struct __attribute__((packed)) BeamDataStruct {
	int ntr;		/* number of real time samples */
	float dtr;		/* real time sampling interval */
	float ftr;		/* first real time sample */
	int nti;		/* number of imaginary time samples */
	float dti;		/* imaginary time sampling interval */
	float fti;		/* first imaginary time sample */
	complex **cf;		/* array[nti][ntr] of complex data */
	complex *dev_cf;		/* cf array point for device use */
} BeamData;

/* one cell in which to linearly interpolate complex time and amplitude */
typedef struct __attribute__((packed)) CellStruct {
	int live;	/* random number used to denote a live cell */
	int dead;	/* random number used to denote a dead cell */
	float tr;	/* real part of traveltime */
	float ti;	/* imaginary part of traveltime */
	float ar;	/* real part of amplitude */
	float ai;	/* imaginary part of amplitude */
} Cell;

/* structure containing information used to set and fill cells */
typedef struct __attribute__((packed)) CellsStruct {
	int nt;		/* number of time samples */
	float dt;	/* time sampling interval */
	float ft;	/* first time sample */
	int lx;		/* number of x samples per cell */
	int mx;		/* number of x cells */
	int nx;		/* number of x samples */
	float dx;	/* x sampling interval */
	float fx;	/* first x sample */
	int lz;		/* number of z samples per cell */
	int mz;		/* number of z cells */
	int nz;		/* number of z samples */
	float dz;	/* z sampling interval */
	float fz;	/* first z sample */
	int live;	/* random number used to denote a live cell */
	int dead;	/* random number used to denote a dead cell */
	float wmin;	/* minimum (reference) frequency */
	float lmin;	/* minimum beamwidth for frequency wmin */
	Cell **cell;	/* cell array[mx][mz] */
	Cell *dev_cell;	/* cell array point for device use */
	Ray *ray;	/* ray */
	BeamData *bd;	/* complex beam data as a function of complex time */
	BeamData *dev_bd;/* beam data stores on teh gpu */
	float **g;	/* array[nx][nz] containing g(x,z) */
	float *dev_g;	/* g array pointer for device use */
} Cells;

/* functions defined and used internally */
static void xtop (float w,
	int nx, float dx, float fx, complex *g,
	int np, float dp, float fp, complex *h);
static BeamData* beamData (float wmin, int nt, float dt, float ft, float *f);
static void setCell (Cells *cells, int jx, int jz);
static void accCell (Cells *cells, Cells *dev_cells, int jx, int jz);
static int cellTimeAmp (Cells *cells, int jx, int jz);
void cellBeam (Cells *cells, Cells *dev_cells, int jx, int jz);

/* Allocating structs on the GPU */
Cells* allocGpuCells ();
void allocGonDevice (int nx, int nz);
void freeGpuCells (Cells *cells, Cells *cellsGpu);
void freeGOnDevice ();
void freeGpuCellsNoG (Cells *cells, Cells *cellsGpu);

void copyCellsHostToDevice (Cells *cells, Cells *dev_cells);
void copyCellsHostToDeviceNoG (Cells *cells, Cells *dev_cell);
void copyOutputGToDevice (float **g, int nx, int nz);
void copyCellsDeviceToHost (Cells *cells);
void copyOutputGToHost (float **g, int nx, int nz);

void checkCells (Cells *cells, Cells *dev_cells);

/* Implementation specific functions that need to be shared */
void cellBeam2CopyFreeSharedCells (Cells *cells, Cells *dev_cells);
void accumulateCellDevice (Cells *cells);

/* Error check code, placed here but should be moved as to not be in the header */

#define gpuErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__);}
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true) {
	if (code != cudaSuccess) {
		warn("GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}
